package com.itb.calendar.presentation.screens.dayofweek;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itb.calendar.R;
import com.itb.calendar.domain.Mp;

public class MpDetailFragment extends Fragment {

    private MpDetailFragmentViewModel mViewModel;
    TextView mpNameView;
    TextView mpTeacherView;
    TextView mpDescriptionView;

    public static MpDetailFragment newInstance() {
        return new MpDetailFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mp7_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MpDetailFragmentViewModel.class);
        // TODO: Use the ViewModel

        int mpNum = MpDetailFragmentArgs.fromBundle(getArguments()).getMpNumber();
        mViewModel.loadModule(mpNum);
        Mp mp = mViewModel.getMp();

        display(mp);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mpNameView = view.findViewById(R.id.mpName);
        mpTeacherView = view.findViewById(R.id.mpTeacher);
        mpDescriptionView = view.findViewById(R.id.mpDescription);
    }

    private void display(Mp mp) {
        String module = "M" + mp.getNumber() + " - " + mp.getName();
        mpNameView.setText(module);
        mpDescriptionView.setText(mp.getDescription());
        mpTeacherView.setText(mp.getTeacher());
    }

}
