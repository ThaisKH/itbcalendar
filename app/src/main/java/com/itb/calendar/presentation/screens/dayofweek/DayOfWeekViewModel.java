package com.itb.calendar.presentation.screens.dayofweek;

import androidx.lifecycle.ViewModel;

import com.itb.calendar.domain.MpSession;
import com.itb.calendar.repository.MpRepository;
import com.itb.calendar.repository.MpRepositoryFactory;

import java.util.List;

public class DayOfWeekViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    List<MpSession> mpSessionList;
    private MpSession mpSession;
    MpRepository mpRepository = MpRepositoryFactory.getInstance();

    public void loadFirstSession(){
        MpRepository mpRepository = MpRepositoryFactory.getInstance();
        mpSession = mpRepository.getClassesForToday().get(0);
    }

    public MpSession getMpSession() {
        return mpSession;
    }

    public void loadSessions(){
        mpSessionList = mpRepository.getClassesForToday();
    }

    public List<MpSession> getMpSessions(){
        return mpSessionList;
    }
}
