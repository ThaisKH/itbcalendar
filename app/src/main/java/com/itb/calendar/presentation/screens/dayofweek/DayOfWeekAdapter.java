package com.itb.calendar.presentation.screens.dayofweek;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.calendar.R;
import com.itb.calendar.domain.MpSession;

import java.util.List;

public class DayOfWeekAdapter extends RecyclerView.Adapter<DayOfWeekAdapter.DayOfWeekViewHolder>{
    List<MpSession> mpSessions;
    OnMpClickListener onMpClickListener;

    public DayOfWeekAdapter(List<MpSession> mpSessions) {
        this.mpSessions = mpSessions;
    }

    public void setOnMpClickListener(OnMpClickListener onMpClickListener) {
        this.onMpClickListener = onMpClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull DayOfWeekViewHolder holder, int position) {
        MpSession mpSession = mpSessions.get(position);
        String module = "M" + mpSession.getMp().getNumber() + " - " + mpSession.getMp().getName();
        holder.mpName.setText(module);
        holder.mpTeacher.setText(mpSession.getMp().getTeacher());
        String time = mpSession.getStartTime() + " - " + mpSession.getEndTime();
        holder.mpTime.setText(time);
    }

    @NonNull
    @Override
    public DayOfWeekViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.mp_row, parent, false);
        return new DayOfWeekViewHolder(view);
    }

    @Override
    public int getItemCount(){
        return mpSessions.size();
    }

    public class DayOfWeekViewHolder extends RecyclerView.ViewHolder {
        TextView mpName;
        TextView mpTeacher;
        TextView mpTime;

        public DayOfWeekViewHolder(@NonNull View itemView){
            super(itemView);

            itemView.setOnClickListener(this::mpClicked);
            mpName = itemView.findViewById(R.id.mpName);
            mpTeacher = itemView.findViewById(R.id.mpTeacher);
            mpTime = itemView.findViewById(R.id.mpTime);
        }

        private void mpClicked(View view){
            MpSession mpSession = mpSessions.get(getAdapterPosition());
            onMpClickListener.onMpClicked(mpSession, view);
        }

    }

    public interface OnMpClickListener{
        void onMpClicked(MpSession mpSession, View view);
    }
}
