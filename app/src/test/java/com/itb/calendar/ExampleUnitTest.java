package com.itb.calendar;

import com.itb.calendar.domain.Mp;
import com.itb.calendar.repository.MpRepository;
import com.itb.calendar.repository.MpRepositoryFactory;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        MpRepository mpRepository = MpRepositoryFactory.getInstance();
        Mp mp = mpRepository.getModuleByNumber(7);
        System.out.println(mp.getTeacher());
        mpRepository.getClassesForToday();
    }
}